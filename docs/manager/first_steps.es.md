## Primeros pasos como manager de una entidad en IsardVDI con usuarios y escritorios persistentes creados.

Esta guía quiere acompañar los primeros pasos de un manager de una entidad que nunca ha usado Isard. Se realizará un recorrido por diferentes aspectos de isard enlazando cuando se necesite con secciones de la documentación donde se puede ampliar la información. 

El punto de partida de esta guía es una entidad creada por el administrador de IsardVDI donde se han creado los siguientes usuarios:

- ***manager***: usuario con el rol **"manager"** i con un escritorio windows persistente creado
- ***profe01, profe02, profe03, profe04, profe05***: cuentas de usuario con el rol **"advanced"** que también tienen un escritorio de windows persistentes creados. Este rol se diferencia del usuario básico en que puede crear plantillas.
- ***test01, test02, , test03, test04, test05***: cuentas de usuario con el rol "**user**" sin ningún escritorio persistente creado

## Acceder por primera vez a IsardVDI

El administrador de IsardVDI nos habrá proporcionado la url de acceso a la plataforma y llegaremos a una pantalla similar a esta:

![](./first_steps_img.es/screenshot_1610338237.png)

Esta interficie la usarán los usuarios para crear escritorios volátiles una vez tengamos creadas las plantillas para ellos. Ahora hay que acceder a la interface de administración, pero primero hay que seleccionar el didioma:

![](./first_steps_img.es/screenshot_1610338477.png)

Después se puede seleccionar nuestra entidad en el desplegable:

![](./first_steps_img.ca/screenshot_1610384459.png)

Y una vez seleccionada se va a la interficie de administracióń haciendo clic en el botón superior de la derecha:

![](./first_steps_img.ca/screenshot_1610384567.png)

Verificamos que la URL contiene el identificador de nuestra entidad. Esta url la podemos pasar a los usuarios avanzados (en nuestro caso profe01, profe02...) para que puedan acceder directamente a la parte de administración:

La url serà similar a esta:

'''
https://url_del_lloc/isard-admin/login/identificador_de_la_entidad
'''

También podemos observar que aparece un texto identificativo de nuestra entidad en la parte superior del formulario de login, como poer ejemplo:

![](./first_steps_img.es/screenshot_1610338748.png)



Ahora ya podemos iniciar sesión como usuario manager y acceder a la interface de administración de IsardVDI:

![](./first_steps_img.es/screenshot_1610338892.png)

En la parte izquierda tenemos un menú, en la parte superior derecha tenemos la información de quotas y en la parte central el listado de "desktops". Se observa que hay un escritorio con el nombre : **win10_T3_with_base_software_persistent**, que hace referencia a un windows 10 en la fase T3 de optimización y con el software libre y gratuito instalado. Si queréis saber como se ha generado esta plantilla, está disponible el mnual de instalación de todos los pasos que se han seguido.

### Arrancar el primer escritorio

Ya podemos arrancar nuestro primer escritorio en Isard, hacemos clic en start y aparece un cuadro de diálogo que nos permite seleccionar el tipo de visor. Podemos acceder con un visor del protocolo spice (hace falta instlaar un software) o bien con una pestaña del navegador. Como es la primera vez que usamos isard podemos usar el visor integrado en el navegador para esta primera prueba. Hacer clic en  "VNC Browser":

![](./first_steps_img.es/screenshot_1610339391.png)

Como está intentando abrir una ventana emergente, hace falta dar permisos. En el caso de firefox hemos de ir al icono ![](./first_steps_img.es/screenshot_1610339485.png) que está a la izquierda de la barra de direcciones:

![](./first_steps_img.es/screenshot_1610339460.png)

y permitir las ventanas emergentess (Allow) i hacer un clic en *Show 1 blocked pop-up*:

![1610339548872](/home/beto/dev/isardvdi-docs/docs/manager/first_steps_img.ca/allow_popup.png) !![1610339697291](/home/beto/dev/isardvdi-docs/docs/manager/first_steps_img.ca/allow_popup2.png)

Y ya podremos trabajar con un windows integrado en el navegador:

![](./first_steps_img.es/screenshot_1610339750.png)

En la parte superiror a la derecha hay dos botones para convertir a pantalla completa y otro para enviar la combinación ctrl + alt + supr al escritorio remoto.

Podemos probar a crear un directorio y verficiar que al cerra y abrir el escritorio se guardan los datos ya que es un escritorio persistente. Siempre es recomendabe, como en los escritorios reales, cerrar el sistema operativo ordenadamente. Procedemos a cerrar el escritorio desde el windows:

![](./first_steps_img.es/screenshot_1610340071.png)

Y observamos como el escritorio cambia en el listado del estado *Started* a *Stopped*:![](./first_steps_img.es/screenshot_1610340109.png)
 --> ![](./first_steps_img.es/screenshot_1610340161.png)

### Visor spice: remote-viewer

Con un visor spice el rendimiento gráfico, la latencia y la interacción con el ratón y el teclado mejora. Es un protocolo que comprime el vídeo, reduce mucho el uso del ancho de banda. 

Podemos instalar el paquete de software **virt-viewer** que incluye el programa **remote-viewer** que usaremos. Web de descarga: https://virt-manager.org/download/

Si usamos windows también podemos instalar el software  **UsbDk** para redirigir el puerto usb.

Hay una explicación más extensa sobre visores en: https://isardvdi.readthedocs.io/en/develop/#user/local-client/

Al seleccionar el cliente SPICE se descarga un fichero con extensión *.vv, que si tienes correctamente instalado el software del visor, abre con el **remote-viewer** el fichero. Al abrir el remote-viewer y acceder al visor del escritorio, se elimina ese fichero vv como una medida de seguridad, por lo que si quieres volver a conectar hay que volver a seleccionar el cliente spice y descargar el fichero de nuevo 

## Crear una plantilla

El proceso para crear una plantilla se ecplica con más detalle en la guía de usuario. Ahora se explicarán los pasos básicos para poder instalar software adicional y crear una plantilla que servirá para crear escritorios a otros usuarios.

Para estre ejemplo explicaremos como instalr el software de cliente de correo Thunderbird. Pasos:

1. Arrancamos el escritorio de windows y nos conectamos a un visor

2. Con una ventana privada del navegador, para que no quede en la maqueta de la plantilla registro del historial de navegación, descargamos el instalador y instalamos Thunderbird:

![](./first_steps_img.es/screenshot_1610341135.png)

3. Aparece la icona del Thunderbird al finalizar la instalación en el escritorio y verificamos que ha funcionado bien.

![](./first_steps_img.es/screenshot_1610341299.png)

4. Apagamos el windows ordenadamente y en el listado de escritorios desplegamos más opciones con el botón ![](./first_steps_img.es/screenshot_1610341353.png)y aparecen las opciones:

![](./first_steps_img.es/screenshot_1610341379.png)

5. Hacemos un lic en **Template it** para crear la plantilla y le damos un nombre y una descripción:

![](./first_steps_img.es/screenshot_1610341545.png)

6. Hacemos clic para desplegar el apartado **Allows** que és el encargado de gestionar los permisos de esta plantilla. Seleccionamos el grupo principal de la entidad denominado**Main**. Activamos **Set groups ** i comenzamos a escribir el inicio de la palabra Main hasta que aparece el listado con el grupo Main y lo seleccionamos.

![](./first_steps_img.es/screenshot_1610341645.png)

Y ya podemos crear la plantilla con el botón: ![](./first_steps_img.es/screenshot_1610341737.png)

7. Vamos al apartado de desktops del menú izquierdo y aparece en el listado de templates:

![](./first_steps_img.es/screenshot_1610341805.png)

### Escriptori volàtil per un usuari amb la plantilla base

Ahora hace falta comprobar que un usuario del grupo *Main* puede iniciar sesión en la interfície principal (no en la de administración) y que le salga para escoger un escritorio volátil basado en la plantilla que acabamos de crear.

1. Vamos a la url principal de la plataforma isard, seleccionamos nuestra entidad y probamos con un usuario normal:

![](./first_steps_img.ca/screenshot_1610387763.png)

2. Aparece una pantalla para seleccionar la plantilla (solamente se muestran las plantillas a las cuales se ha dado acceso).

![](./first_steps_img.ca/screenshot_1610401348.png)

3. Al hacer un clic se crea un escritorio virtual volátil y te da acceso al visor integrado en el navegador o con el cliente local (spice), te reconoce el sistema operativo desde donde te conectas y te presenta una guía si haces un clic en la palabra **"aquí"**:
![](./first_steps_img.ca/screenshot_1610401472.png)

Y aparece una guía adaptada al sistema operativo del cliente donde explica como se ha de hacer para instalar el cliente local. Por ejemplo si estoy con un ubuntu saldrá esta guía con las opciones:



![](./first_steps_img.ca/screenshot_1610401552.png)

y si usas windows aparece esta otra guía con un enlace directo al instalador

![](./first_steps_img.ca/screenshot_1610401856.png)

Y con el visor que queramos ya podemos comenzar a trabajar con este escritorio. En este caso se trata de un escritorio volátil, el resultado del trabajo se ha de guardar en algún recurso de internet como espacio en la nube, curso de moodle o un correo electrónico... si se hace logout de IsardVDI se destruye el escritorio volátil.

