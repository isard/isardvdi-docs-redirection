## Primeres passes com a manager d'una entitat a IsardVDI amb usuaris i escriptoris persistents creats.

Aquesta guia vol acompanyar les primeres passes d'un manager d'una entitat que mai ha fet servir Isard. Es farà un recorregut per diferents aspectes d'Isard enllaçant quan calqui amb altres seccions de la documentació.

El punt de partida d'aquesta guia és una entitat creada per l'administrador d'IsardVDI on s'han creat els següents usuaris:

- ***manager***: usuari amb rol **"manager"** i amb un escritori de windows persistent creat
- ***profe01, profe02, profe03, profe04, profe05***: comptes d'usuari amb rol **"advanced"** que també tenen un escriptori de windows persistent creat. Amb aquest rol es poden crear plantilles que poden fer servir la resta d'usuaris.
- ***alumne01, alumne02, , alumne03, alumne04, alumne05***: comptes d'usuari amb rol "**user**" sense cap escriptori persistent creat

## Accedir per primer cop a IsardVDI

L'administrador d'isard ens haurà proporcionat la url d'accés a la plataforma i accedirem a una pantalla similar a aquesta:

![](./first_steps_img.ca/screenshot_1610338237.png)

Aquesta interfície la faran servir els usuaris per crear escriptoris volàtils una vegada tinguem creades les plantilles. Ara haurem d'accedir a la interfície d'administració. Primer podem seleccionar l'idioma:

![](./first_steps_img.ca/screenshot_1610338477.png)

Després podem seleccionar la nostra entitat al desplegable:

![](./first_steps_img.ca/screenshot_1610338531.png)

I una vegada seleccionada anirem a administració fent clic al botó superior dreta:

![](./first_steps_img.ca/screenshot_1610338596.png)

Verifiquen que la url conté l'identificador de la nostra entitat. Aquesta url la podem passar als usuaris avançats (el nostre cas profe01, profe02...) perquè puguin accedir directament a la part d'administració:

La url serà semblant a:

'''
https://url_del_lloc/isard-admin/login/mila_i_fontanals
'''

També podem observar que apareix un text identificatiu de la nostra entitat al login:

![](./first_steps_img.ca/screenshot_1610338748.png)

Ara ja podem iniciar sessió com a usuari manager i accedim a la interfície d'administració d'Isard.

![](./first_steps_img.ca/screenshot_1610338892.png)

A la part esquerra tenim un menú, en la part superior dreta tenim la informació de quotes i en la part central el llistat de "desktops". Observem que hi ha un escriptori amb el nom: **win10_T3_with_base_software_persistent**, que fa referència a un windows 10 amb la fase T3 d'optimització i amb software lliure i gratuït instal·lat. Si voleu saber com s'ha generat aquesta plantilla base està disponible el manual d'instal·lació pas a pas amb les diferents fases.

### Arrencar el nostre primer escriptori

Ja podem arrencar el nostre primer escriptori a Isard, fem clic a start i apareix un quadre de diàleg que ens permet seleccionar el tipus de visor. Podem accedir amb un visor del protocol spice (cal instal·lar software) o bé amb una pestanya del navegador. Com és la primera vegada que fem servir Isard recomanem fer servir el visor integrat al navegador per fer una primera prova. Fem clic a "VNC Browser":

![](./first_steps_img.ca/screenshot_1610339391.png)

Com està intentant obrir una finestra emergent, cal donar permisos. En el cas de firefox hem d'anar a l'icona ![](./first_steps_img.ca/screenshot_1610339485.png) que està a l'esquerra de la barra de dierccions:

![](./first_steps_img.ca/screenshot_1610339460.png)

i permetre les finestres emergents (Allow) i fer un clic a *Show 1 blocked pop-up*:

![1610339548872](/home/beto/dev/isardvdi-docs/docs/manager/first_steps_img.ca/allow_popup.png) !![1610339697291](/home/beto/dev/isardvdi-docs/docs/manager/first_steps_img.ca/allow_popup2.png)

I ja podem treballar amb un windows integrat al navegador:

![](./first_steps_img.ca/screenshot_1610339750.png)

A la part de dalt a la dreta hi ha dos botons per convertir a pantalla sencera i un altre per enviar la combinació ctrl+alt+supr a l'escriptori remot.

Podem provar de crear un directori i verificar que al tancar i obrir l'escriptori es guarden les dades, ja que és un escriptori persistent. Sempre es recomanable, com als escriptoris reals, tancar l'escriptori ordenadament. Procedim a tancar l'escriptori des de windows:

![](./first_steps_img.ca/screenshot_1610340071.png)

I observem com l'escriptori canvia al llistat d'estat d'Started a Stopped:

![](./first_steps_img.ca/screenshot_1610340109.png) --> ![](./first_steps_img.ca/screenshot_1610340161.png)

### Visor spice: remote-viewer

Amb un visor spice el rendiment gràfic, la latència i la interacció amb el ratolí i el teclat millora. És un protocol que comprimeix el vídeo i redueix molt l'amplada de banda per poder treballar. A més a més, el protocol spice ens permet connectar dispositius USB del client a dins de l'escriptori, també ens permet fer servir el portapapers y fer Copy/Paste dins i fora de l'escriptori.

Podem instal·lar el paquet de software virt-viewer que inclou el remote-viewer a la web: https://virt-manager.org/download/

Si fem servir windows també podem instal·lar el software **UsbDk** per redirigir port usb.

Hi ha una explicació més extensa sobre visors a: https://isardvdi.readthedocs.io/en/develop/#user/local-client/

Al fer clic al botó de  client SPICE es descarrega un fitxer amb extensió *.vv, que si tens correctament instal·lat el programari del visor, obre amb el **remote-viewer** el fitxer. En obrir el remote-viewer i accedir al visor de l'escriptori, s'esborra aquest fitxer vv com una mesura de seguretat. Si vols tornar a connectar cal tornar a seleccionar el client spice i descarregar el fitxer de nou.

## Crear una plantilla

Per crear una plantilla s'explica amb més detall a la guia d'usuari. Ara farem les pases básiques per poder instal·lar software adicional i crear una plantilla que servirà per crear escriptoris a altres usuaris.

Per aquest exemple explicarem com instal·lar el software de client de correu thunderbird. Passes:

1. Arranquem l'escriptori de windows i ens connectem a un visor.

2. Amb una finestra privada del navegador, per no deixar historial de navegació a la maqueta, descarreguem l'instal·lador i instal·lem el Thunderbird:

![](./first_steps_img.ca/screenshot_1610341135.png)

3. Apareix la icona del Thunderbird al finalitzar la instal·lació a l'escriptori i verifiquem que ha funcionat bé.

![](./first_steps_img.ca/screenshot_1610341299.png)

4. Apaguem el windows ordenadament i al llistat de l'escriptori despleguem més opcions amb el botó ![](./first_steps_img.ca/screenshot_1610341353.png) i apareixen les opcions:

![](./first_steps_img.ca/screenshot_1610341379.png)

5. Fem un clic a **Template it** per crear la plantilla i li donem un nom i una descripció:

![](./first_steps_img.ca/screenshot_1610341545.png)

6. Fem clic per desplegar l'apartat **Allows** que és l'encarregat de gestionar els permisos d'aquesta plantilla. Seleccionem el grup principal de l'entitat denominat **Main**. Activem **Set groups ** i comencem a escriure "ma" fins que apareix el grup i el seleccionem.

![](./first_steps_img.ca/screenshot_1610341645.png)

Y ara ja podem crear la plantilla: ![](./first_steps_img.ca/screenshot_1610341737.png)

7. Anem a l'apartat template del menú i apareix al llistat de templates:

![](./first_steps_img.ca/screenshot_1610341805.png)

### Escriptori volàtil per un usuari amb la plantilla base

Ara cal provar que un usuari del grup pot iniciar sessió a la interfície principal (no a la d'administració) i li surt per escollir un escriptori volàtil basat en la nostra plantilla.

1. Anem a la url principal de la plataforma isard, seleccionem la nostra entitat i provem amb un usuari normal:

![](./first_steps_img.ca/screenshot_1610342297.png)

2. Apareix una pantalla per seleccionar la plantilla (solament es mostren les plantilles a les quals s'ha donat accés) .

![](./first_steps_img.ca/screenshot_1610342382.png)

3. Al fer un clic es crea un escriptori virtual volàtil i et dóna accés al visor integrat al navegador o amb el client local (spice), et reconeix el teu sistema operatiu i hi ha una guia fent un clic a la paraula **"aquí"**:
![](./first_steps_img.ca/screenshot_1610342525.png)

i apareix una guia adaptada al sistema operatiu del client on explica com s'ha de fer per instal·lar el client local. Per exemple si estic amb un ubuntu sortirà aquesta guia amb les opcions:

![](./first_steps_img.ca/screenshot_1610342566.png)

i si fas servir windows surt aquesta altra guia amb un enllaç directa a l'instal·lador:

![](./first_steps_img.ca/screenshot_1610342804.png)

I amb el visor que vulguem ja podem començar a treballar amb aquest escriptori. En aquest cas es tracta d'un escriptori volàtil, el resultat del treball s'ha de guardar a algun núvol d'internet, tasca de moodle o correu electrònic... si es fa logout es destrueix l'escriptori.

