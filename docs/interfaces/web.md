# Web Interfaces

From version 2.0 there are two interfaces that gives different access types to IsardVDI, the [*full administration interface*](#full-administration-web-interface) and a new [*simplified user interface*](#simplified-user-web-interface). This allows advanced users and new **manager** users to administer templates and company limits and quotas while leaving a simplified interface to the users.

## Simplified user web interface

This simplified interface is the main interface when user access to the domain URL root, Eg. <https://desktops.isardvdi.com>. It is intended to bring a simplified user interface to access non-persistent desktops from its user allowed templates. We will extend it to allow manage persistent desktops in the future.

![nonpersistent_login](../images/first-steps/nonpersistent_login.png)

This interface allows:

- Access with local users or using Oauth2 authentications like Google and Github accounts. You should set up your application auth tokens in isardvdi.cfg.
- Access to live nonpersistent desktops, destroyed on session logout.
- Multi template option

Managers and administrators must configure IsardVDI categories and groups to make simplified user interface work ([follow next section](#full-administration-web-interface)).

![nonpersistent_login](../images/first-steps/nonpersistent_access.png)

With this simple interface the user is given two kinds of viewers:

- Local client: Using the spice client
- Browser: Using the same browser with HTML5

If the user has access to more than one non-persistent desktop (he has access to more than one template) the system will ask which non persistent desktop wants to create and access on login:

![nonpersistent_login](../images/first-steps/enrollment_templates.png)

## Full administration web interface

This interface is intended for **Administrator**, **Manager** and **Advanced** roles as it brings them full control. Normal **users** can still access this admin interface if they are allowed to have persistent desktops.

The URL for accessing to this web interface for *Default* category is in **/isard-admin** if you don't create a multitenancy configuration. E.g. <https://desktops.isardvdi.com/isard-admin>.

![nonpersistent_login](../images/first-steps/login_default.png)

Once inside the administator can create a **multitenant** acces by creating categories. For multitenancy configurations each user should access through it's own category portal in **/isard-admin/login/[category]**.

![nonpersistent_login](../images/first-steps/login_acme.png)
![](../images/first-steps/multitenant.png)

When you create a new category (entity) it will create a default 'Main' group for that category. You can create as many groups in that category as you want. And in groups you will find the auto registering keys for OAUTH users:

![](../images/first-steps/groups.png)

The category groups have the enrollment keys for the users. You can always create users in that category in local database by using this **users** menu, but you can also use those auto enrollment keys and distribute it to IsardVDI users to let them auto enroll in a group.

Groups and Categories can have absolute limits set up. Also they can have a per user quota set, that can be overriden in individual users.

![](../images/first-steps/groups.png)

And there you will be able to enable/disable role enrollment keys and generate new ones.

![](../images/first-steps/enrollment.png)

## Sharing templates

As the simplified user interface only allows (by now) non-persistent desktops, it will show to the user the templates he has access and it will create a non-persistent desktop ready to be used that will be destroyed on session logout.

So be sure to share (alloweds) at least one template with the user (or his group o category).
